import EmberObject from '@ember/object';
import Controller from '@ember/controller';
import { A } from '@ember/array';
import faker from 'faker';

export default Controller.extend({
  tableConfig: null,
  init() {
    this._super(...arguments);

    var object = EmberObject.extend({});

    var inputConfig = object.create();
    inputConfig.set("label", "New Input Value");
    inputConfig.set("searchPlaceHolder", "New Input Value");
    inputConfig.set("value",null);
    inputConfig.set("disabled", false);
    inputConfig.set("fixedDecimalPlaces", 3);

    this.set("inputConfig", inputConfig);

  }
});
