import Component from '@ember/component';

export default Component.extend({
  init() {
    this._super(...arguments);
    this.send('roundInput');
  },
  tagName: '',
  nullable: false,
  oldValue: '',
  oldCharStr: '',
  getMaximum: function(value){
    var maximum = this.get("max");
    var decimalPlaces = this.get("fixedDecimalPlaces");
    if (maximum !== "" && maximum!== null && maximum !== undefined){
      var maximum = Number(maximum);
      if (value > maximum){
        value = maximum.toFixed((decimalPlaces))
      }
    }
    return value
  },
  getMinimum: function(value){
    var minimum = this.get("min");
    var decimalPlaces = this.get("fixedDecimalPlaces");
    if (minimum !== "" && minimum !== null && minimum !== undefined){
      var minimum = Number(minimum);
      if (value < minimum){
        value = minimum.toFixed((decimalPlaces))
      }
    }
    return value
  },
  actions:{
    roundInput(){
      if((this.get("value") !== "" && this.get("value") !== null) || this.get("nullable") === false){
        var decimalPlaces = this.get("fixedDecimalPlaces");
        decimalPlaces = (decimalPlaces !== "" && decimalPlaces !== null && decimalPlaces !== undefined) ? Number(decimalPlaces) : 2;
        var rounded = Number(this.get('value')).toFixed(decimalPlaces);
        rounded = this.getMaximum(rounded);
        rounded = this.getMinimum(rounded);
        this.setProperties({'value': rounded});
        this.rerender();
      }
      else{
        this.setProperties({'value': ""});
      }

    },

    keyUp(e) {
      var minimum = Number(this.get("min"));
      var maximum = Number(this.get("max"));
      var pattern = new RegExp(/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/);
      var newValue = this.get("value");
      var oldValue = this.get("oldValue");
      var passes = newValue.match(pattern);
      var rounded = Number(newValue);

      if ((!passes && (e.keyCode !== 8 && newValue !== "" && newValue !== "+" && newValue !== "-")) || (e.keyCode !== 8 && this.get("oldCharStr") === "." && newValue === "") || ((newValue !== "" && rounded < minimum) || (newValue !== "" && rounded > maximum))){
        this.set("value", oldValue);
      } else {
        this.set("oldValue", this.get("value"));
      }
    },
    keyPress(evt){
      var oldValue = this.get("value");
      var pattern = new RegExp(/^[+-]?((\d+(\.\d*)?)|(\.\d+))$/);
      evt = evt || window.event;
      var charCode = evt.keyCode || evt.which;
      var charStr = String.fromCharCode(charCode);
      this.set("oldCharStr", charStr);
      var newValue = oldValue + charStr;
      var passes = newValue.match(pattern);
      if (!passes && (newValue !== "" && newValue !== "+" && newValue !== "-")){
        evt.preventDefault();
      }

    },
    focusIn(){
      this.send('roundInput');
      if(this.get('focus-in')){
        this.get('focus-in')()
      }
    },
    focusOut(){
      this.send('roundInput');
      if(this.get('focus-out')){
        this.get('focus-out')()
      }
    }
  }
});
